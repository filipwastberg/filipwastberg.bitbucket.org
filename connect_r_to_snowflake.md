---
title: "Connecting R to Snowflake"
author: "Filip W�stberg"
date: '2018-03-23'
output:
  html_document:
    keep_md: yes
---

## Background
Since bitbucket does not support Swedish encoding this tutorial will be in English.

### Some introductionary remarks
R is connected to Snowflake through Java Database Connectivity (JDBC). Since Java is not pre installed on Mac OSX computers you will need to download Java and a JDBC-driver. If you're on a mac, just follow [these](https://support.snowflake.net/s/article/connecting-r-to-snowflake-using-the-jdbc-driver-mac-os-x) instructions.

If you're on a Windows 32bit there a instructions [here](https://support.snowflake.net/s/article/connecting-r-to-snowflake-using-32-bit-odbc-driver-windows).

## Setup
We load the packages needed and specify the jdbc.classpath to where your jdbc-driver is. For me it's in downloads.

dplyr is found on cran through `install.packages("dplyr")` and `dplyr.snowflakedb` on github through `devtools` `devtools::install_github("snowflakedb/dplyr-snowflakedb")`


```r
library(dplyr.snowflakedb)
library(dplyr)
options(dplyr.jdbc.classpath = "/Users/filipwastberg/Downloads/snowflake-jdbc-3.5.3.jar")
```

### Set up connection
In the dplyr.snowflakdb package there is a function for connecting to a database. To make this connection you need a username and password, also it's important to specify the region. 

```r
my_db <- src_snowflakedb(user = "user",
                         password = "password",
                         account = "ferrologic",
                         region = "eu-central-1",
                         opts = list(warehouse = "SWESKI_ANALYTICS",
                                     db = "SWESKI_DB",
                                     schema = "SWESKI_SCHEMA"))
```

```
## host:
```

```
## URL: jdbc:snowflake://ferrologic.eu-central-1.snowflakecomputing.com:443/?account=ferrologic&warehouse=SWESKI_ANALYTICS&db=SWESKI_DB&schema=SWESKI_SCHEMA
```

We now have a connection to sweski database.

What tables do we have in this database? Let's have a look. You need to specify `schema`.


```r
dbListTables(my_db$con, schema = "%SWESKI_SCHEMA")
```

```
##  [1] "COMPETITOR"         "COMPETITOR_HISTORY" "CUPRESULT"         
##  [4] "DISCIPLINE"         "EVENT"              "LISTDEFAL"         
##  [7] "LISTRESULTAL"       "RACEAL"             "RESULTAL"          
## [10] "test_resultal"
```

Let's have a look at one of the tables. This is easily done with `tbl()`.


```r
tbl(my_db, "RESULTAL") %>%
  head()
```

```
## # Source:   lazy query [?? x 17]
## # Database: SnowflakeDB Data Source
##      RECID RACEID COMPETITORID COMPETITORNAME   POSITION STATUS   BIB
##      <dbl>  <dbl>        <dbl> <chr>               <dbl> <chr>  <dbl>
## 1 5397516. 92888.      203603. KANG Chang-Yeon       44. QLF      57.
## 2 5397056. 91655.      215091. DANCIU Raul            0. DNS1    109.
## 3 5397004. 91655.       68886. ZRNCIC DIM Natko      15. QLF      51.
## 4 5397573. 92888.      198139. YAMADA Kai             0. DNF1    109.
## 5 5395094. 92101.      190874. GIANNOTTI Chiara       0. DNS2     41.
## 6 5397651. 92887.      187410. KAWABATA Kaede         0. DNF2      6.
## # ... with 10 more variables: NATIONCODE <chr>, TIMER1 <chr>,
## #   TIMER2 <chr>, TIMER3 <chr>, TIMETOT <chr>, VALID <dbl>,
## #   RACEPOINTS <dbl>, CUPPOINTS <dbl>, LASTUPDATE <chr>, LOADDATE <chr>
```

Behind the scenes we are actually running a SQL query, we can get that query through `get_query()`


```r
tbl(my_db, "RESULTAL") %>%
  head() %>%
  show_query()
```

```
## <SQL>
## SELECT *
## FROM "RESULTAL"
## LIMIT 6
```

So the data isn't in R:s memory, we are simply running R code on Snowflake through SQL.

Thus, we can do more complex queryies.


```r
cupresult <- tbl(my_db, "CUPRESULT")
competitor <- tbl(my_db, "COMPETITOR")

cupresult %>%
  left_join(competitor %>% select(COMPETITORID, NATIONCODE), by = c("COMPETITORID")) %>%
  filter(SEASONCODE == 2018 & CUPID == "WC" & SECTORCODE == "AL") %>%
  group_by(NATIONCODE) %>%
  summarise(points_per_country = sum(POINTS, na.rm = TRUE) / n()) %>%
  arrange(desc(points_per_country))
```

```
## # Source:     lazy query [?? x 2]
## # Database:   SnowflakeDB Data Source
## # Ordered by: desc(points_per_country)
##    NATIONCODE points_per_country
##    <chr>                   <dbl>
##  1 LIE                      444.
##  2 NOR                      187.
##  3 SWE                      172.
##  4 AUT                      154.
##  5 SVK                      149.
##  6 SUI                      143.
##  7 ITA                      131.
##  8 USA                      125.
##  9 GER                      117.
## 10 GBR                      114.
## # ... with more rows
```

Given the sparse number of comptetitors Lichtenstein provide (from their 37 000 inhabitants) the small alp country is performing well.

Let's have a look at the query.


```r
cupresult %>%
  left_join(competitor %>% select(COMPETITORID, NATIONCODE), by = c("COMPETITORID")) %>%
  filter(SEASONCODE == 2018 & CUPID == "WC" & SECTORCODE == "AL") %>%
  group_by(NATIONCODE) %>%
  summarise(points_per_country = sum(POINTS, na.rm = TRUE) / n()) %>%
  arrange(desc(points_per_country)) %>%
  show_query()
```

```
## <SQL>
## SELECT "NATIONCODE", SUM("POINTS") / COUNT(*) AS "points_per_country"
## FROM (SELECT "TBL_LEFT"."RECID" AS "RECID", "TBL_LEFT"."SECTORCODE" AS "SECTORCODE", "TBL_LEFT"."SEASONCODE" AS "SEASONCODE", "TBL_LEFT"."CUPID" AS "CUPID", "TBL_LEFT"."DISCIPLINECODE" AS "DISCIPLINECODE", "TBL_LEFT"."COMPETITORID" AS "COMPETITORID", "TBL_LEFT"."GENDER" AS "GENDER", "TBL_LEFT"."RANK" AS "RANK", "TBL_LEFT"."POINTS" AS "POINTS", "TBL_LEFT"."LASTUPDATE" AS "LASTUPDATE", "TBL_LEFT"."LOADDATE" AS "LOADDATE", "TBL_RIGHT"."NATIONCODE" AS "NATIONCODE"
##   FROM "CUPRESULT" AS "TBL_LEFT"
##   LEFT JOIN (SELECT "COMPETITORID", "NATIONCODE"
## FROM "COMPETITOR") "TBL_RIGHT"
##   ON ("TBL_LEFT"."COMPETITORID" = "TBL_RIGHT"."COMPETITORID")
## ) "tzawhzfihr"
## WHERE ("SEASONCODE" = 2018.0 AND "CUPID" = 'WC' AND "SECTORCODE" = 'AL')
## GROUP BY "NATIONCODE"
## ORDER BY "points_per_country" DESC
```

Antoher thing we might is to visualize data from Snowflake. A fast and neat way to do this is with with the package `dbplot`. Let's visualize the geneder distribution 


```r
library(dbplot)
library(ggplot2)

cupresult %>%
  left_join(competitor %>% select(COMPETITORID, NATIONCODE), by = c("COMPETITORID")) %>%
  filter(GENDER %in% c("L", "M")) %>%
  dbplot_bar(GENDER)
```

![](connect_r_to_snowflake_files/figure-html/dbplot-1.png)<!-- -->

This is neat but we'd like to manipulate this a little bit more. For this purpose we have a family of functions called `db_compute` that we can use with ggplot2. 


```r
library(dbplot)
library(scales)

space <- function(x, ...) { 
  format(x, ..., big.mark = " ", scientific = FALSE, trim = TRUE)
}

cupresult %>%
  left_join(competitor %>% select(COMPETITORID, NATIONCODE), by = c("COMPETITORID")) %>%
  filter(GENDER %in% c("L", "M")) %>%
  db_compute_count(GENDER) %>%
  rename(n = "n()") %>%
  ggplot() +
  geom_col(aes(x = GENDER, y = n, fill = GENDER)) +
  labs(title = "Gender distribution of comptetitors",
       x = "Gender", y = "") +
  scale_y_continuous(labels = space) +
  theme_minimal()
```

![](connect_r_to_snowflake_files/figure-html/dbcompute-1.png)<!-- -->














